Vue.component('Datepicker', {
    template:
'    <div data-tpl="Datepicker">' +
'        <div>' +
'            <div class="col-sm-5">' +
'                <input type="text" class="form-control" id="datepicker" />' +
'            </div>' +
'        </div>' +
'    </div>',
    props: ['msg'],
    data: function() {
        return {
            //Data
            date: '',
            //Style
            root: 'col-lg-12',
            body: '',
            in_text: 'form-control',
            btn: 'btn btn-primary'
        }
    },
    methods: {
        'switchPage': function() {
            this.funBIndex = event.target.innerText;
        }
    },
    ready : function() {
        $( "#datepicker" ).datepicker();
    }
})
