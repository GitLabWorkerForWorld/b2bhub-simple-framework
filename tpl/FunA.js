Vue.component('FunA', {
    template:
'   <div data-tpl="FunA" :class="root">' +
'     <div :class="body">' +
'        <form class="form-horizontal" id="login">'+
'            <div class="form-group has-success has-feedback">'+
'                <label class="col-sm-2" for="exampleInputName2">Name</label>'+
'                <div class="col-sm-8">'+
'                    <input class="form-control" type="text" name="name">'+
'                    <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>'+
'                    <span id="inputSuccess2Status" class="sr-only">(success)</span>'+
'                </div>'+
'            </div>'+
'            <div class="form-group has-warning has-feedback">'+
'                <label class="col-sm-2" for="exampleInputName2">nick name</label>'+
'                <div class="col-sm-8">'+
'                    <input class="form-control" type="text" name="pwd">'+
'                    <span class="glyphicon glyphicon-warning-sign form-control-feedback" aria-hidden="true">'+
'                    </span>'+
'                    <span id="inputWarning2Status" class="sr-only">(warning)</span>'+
'                </div>'+
'            </div>'+
'            <div class="form-group">'+
'                <label class="col-sm-2">select</label>'+
'                <div class="col-sm-8">'+
'                <select class="form-control" name="number">'+
'                  <option>1</option>'+
'                  <option>2</option>'+
'                  <option>3</option>'+
'                  <option>4</option>'+
'                  <option>5</option>'+
'                </select>'+
'                </div>'+
'            </div>'+
'            <div class="form-group">'+
'                <div class="col-sm-offset-5">'+
'                  <button type="submit" class="btn btn-primary" @click=submit>Sign in</button>'+
'                </div>'+
'            </div>'+
'        </form>'+
'     </div>' +
'   </div>',
    props: ['msg'],
    data: function() {
        return {
            //Data
            msg: '',
            //Style
            root: 'FunA-style',
            body: 'container-fluid',
            in_text: '',
            btn: ''
        }
    },
    methods: {
        'submit': function() {
            $("#login").submit(function(event) {
              console.log($(this).serializeArray());
              event.preventDefault();
            });
        }
    }
})
