Vue.component('FunB', {
    template:
    '<div data-tpl="FunB" :class="body">' +
    '    <ul class="nav nav-tabs">' +
    '      <li><a href="#" @click="switchPage">FunB-Home</a></li>' +
    '      <li><a href="#" @click="switchPage">FunB-Menu</a></li>' +
    '    </ul>' +
    '   <component :is="funBIndex"></component>' +
    '</div>',
    props: ['msg'],
    data: function() {
        return {
            //Data
            funBIndex: 'FunB-Home',
            //Style
            root: 'col-lg-12',
            body: '',
            in_text: 'form-control',
            btn: 'btn btn-primary'
        }
    },
    methods: {
        'switchPage': function() {
            this.funBIndex = event.target.innerText;
        }
    }
})
