Vue.component('index', {
    template:
        '<div data-tpl="index">' +
        '    <navbar :child.sync=child></navbar>' +
        '    <sidebar :child.sync=child></sidebar>' +
        '    <div id="page-wrapper">' +
        '       <div :class="container">' +
        '           <component :is="child" ' +
        '                       transition="fade" ' +
        '                       transition-mode="out-in" ' +
        '                       :msg.sync=msg' +
        '                       :funMsg=funMsg >' +
        '           </compnent>' +
        '       </div>' +
        '    </div>' +
        '</div>',
    data: function() {
        return {
            //Data
            child: 'FunA',
            msg: '',
            funMsg: '',
            //Style
            container: 'main-area'
        }
    },
    props: ['msg']
})
