Vue.component('Modal', {
    template:
'    <div class="container" data-tpl="Modal">' +
'        <h2>Modal Login Example</h2>'+
'        <!-- Trigger the modal with a button -->'+
'        <button type="button" class="btn btn-default btn-lg" @click=switchPage>Login</button>'+
'        <component :is="index" ' +
'                   @child-switch="switchChild" ' +
'                   @return-data=returnData ' +
'                   :username=username ' +
'                   :pwd=pwd>' +
'        </compnent>' +
'    </div>',
    data: function() {
        return {
            //Data
            index: 'Modal-Home',
            username : '',
            pwd: ''
        }
    },
    methods: {
        'switchPage': function() {
            this.index = 'Modal-' + event.target.innerText;
        },
        'switchChild': function(msg) {
            this.index = 'Modal-' + msg;
        },
        'returnData': function(obj) {
            this.username = obj.ac;
            this.pwd = obj.pwd;
        }
    },
    ready : function() {
        this.index = 'Modal-Home';
    }
})
