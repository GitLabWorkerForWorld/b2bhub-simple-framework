Vue.component('navbar', {
    template:
    '<div data-tpl="navbar" v-bind:class="root">' +
    '       <ul v-bind:class="ul">' +
    '           <li>' +
    '               <a href="#" @click="switchPage" v-bind:class="[item, item_on]">page1</a>' +
    '           </li>' +
    '           <li>' +
    '               <a href="#" @click="switchPage" v-bind:class="[item]">page2</a>' +
    '           </li>' +
    '       </ul>' +
    '</div>',
    props: ['child'],
    data: function() {
        return {
            //Set element css class name.
            root: 'navbar navbar-static-top',//tab style
            body: '',//navbar style
            ul: 'nav navbar-top-links navbar-right',//ul style
            item: '',//item style
            item_on: ''//item_on style
        }
    },
    methods: {
        'switchPage': function() {
            //console.log(event.target);
            //Change page view components via user trigger event.
            this.child = event.target.innerText;
            //Use jquery selector to change navbar style(on or off).
            $(event.target).addClass('weui_bar_item_on').
                siblings('.weui_bar_item_on').
                    removeClass('weui_bar_item_on');
        }
    }
})
