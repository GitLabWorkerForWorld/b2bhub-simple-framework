Vue.component('sidebar', {
    template:
    '<div id="sidebar" data-tpl="sidebar" v-bind:class="root">' +
    '    <div v-bind:class="body">' +
    '       <ul v-bind:class="ul" id="side-menu">' +
    '           <li><a @click="switchPage" href="#">FunA</a></li>' +
    '           <li><a @click="switchPage" href="#">FunB</a></li>' +
    '           <li><a @click="switchPage" href="#">Datepicker</a></li>' +
    '           <li><a @click="switchPage" href="#">Modal</a></li>' +
    '           <li><a @click="switchPage" href="#">Handsontable</a></li>' +
    '       </ul>' +
    '    </div>' +
    '</div>',
    props: ['child'],
    data: function() {
        return {
            //Set element css class name.
            root: 'sidebar',//tab style
            body: 'sidebar-nav navbar-collapse',//navbar style
            ul: 'nav',//ul style
            item: '',//item style
            item_on: '',//item_on style
            icon: ''
        }
    },
    methods: {
        'switchPage': function() {
            //console.log(event.target);
            //Change page view components via user trigger event.
            this.child = event.target.innerText;
        }
    },
    ready : function() {
        //Use metisMenu() to build sidebar.
        $('#sidevbar').metisMenu();
    }
})
