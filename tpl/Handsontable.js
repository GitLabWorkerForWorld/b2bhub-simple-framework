Vue.component('Handsontable', {
    template:
'    <div data-tpl="Handsontable" class="scroll-table">' +
'       <ul class="nav nav-tabs">' +
'           <li><a href="#" @click=set>set</a></li>' +
'           <li><a href="#" @click=reset>reset</a></li>' +
'           <li><a href="#" @click=export>export</a></li>' +
'       </ul>' +
'       <div id="example-handsotable" class="hot handsontable htRowHeaders htColumnHeaders"></div>' +
'    </div>',
    props: [],
    data: function() {
        return {
            excel: [
                ["2016", 10, 11, 12, 13],
                ["2017", 20, 11, 14, 13],
                ["2018", 30, 15, 12, 13]
            ],//Handsontable's data.
            ht: {}//Handsontable's instance.
        }
    },
    methods: {
        'set': function() {
            var excelData = JSON.parse(JSON.stringify(this.excel));
            this.ht.loadData(excelData);
        },
        'reset': function() {
            this.ht.clear();
            this.ht.loadData([[]]);
        },
        'export': function() {
            var exportPlugin = this.ht.getPlugin('export');
            console.log(exportPlugin);
            //exportPlugin.downloadFile('csv', {filename: 'MyFile'});
        }
    },
    ready : function() {
        var vm = this;
        var el = document.getElementById('example-handsotable');
        this.ht = new Handsontable(el, {
          data: [[]],
          minRows: 30,
          minCols: 30,
          rowHeaders: true,
          colHeaders: true,
          renderAllRows: true,
          dropdownMenu: true,
          columnSorting: true,
          sortIndicator: true,
          stretchH: 'all',
          height: 480
/*
          afterChange: function() {
              console.log(vm.excel);
          }
*/
        });
    }
});
