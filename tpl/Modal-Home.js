Vue.component('Modal-Home', {
    template:
'    <div class="container" data-tpl="Modal-Home">' +
'      <form id="login" class="form-horizontal" @submit.prevent="onSave">' +
'            <div class="form-group has-feedback">'+
'                <label class="col-sm-2" for="exampleInputName2">User Name</label>'+
'                <div class="col-sm-5">'+
'                    <input class="form-control" type="text" name="useranme" v-model="account">'+
'                    <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>'+
'                </div>'+
'            </div>'+
'            <div class="form-group has-feedback">'+
'                <label class="col-sm-2" for="exampleInputName2">pwd</label>'+
'                <div class="col-sm-5">'+
'                    <input class="form-control" type="text" name="pwd" v-model="password">'+
'                    <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>'+
'                </div>'+
'            </div>'+
'        <button @click="onSave" class="btn btn-primary">Save</button> ' +
'      </form>' +
'    </div>',
    props : ['username', 'pwd'],
    data: function() {
        return {}
    },
    methods: {
        'onSave': function() {
            this.$emit('return-data', {'ac': this.username, 'pwd': this.pwd});
        }
    },
    computed: {
        account: {
            get: function() {
                return this.username;
            },
            set: function(val) {
                this.$emit('return-data', {'ac': val.trim(), 'pwd': this.pwd});
            }
        },
        password: {
            get: function() {
                return this.pwd;
            },
            set: function(val) {
                this.$emit('return-data', {'ac': this.username, 'pwd': val.trim()});
            }
        }
    }
})
