Vue.component('FunB-Home', {
    template: '<div data-tpl="FunB-Home" :class="body">' +
    '<div>' +
    '<p>I\'m a FunB-Home page</p>' +
    '</div>',
    props: ['msg'],
    data: function() {
        return {
            //Data
            msg: '',
            //Style
            root: 'col-lg-12',
            body: '',
            in_text: 'form-control',
            btn: 'btn btn-primary'
        }
    },
    methods: {
        'notify': function() {
            console.log('test');
        }
    }
})
