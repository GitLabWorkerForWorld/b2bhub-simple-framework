Vue.component('page2', {
    template:
        '<div data-tpl="page2" :class="body">' +
        '   <input v-model="msg" :class="in_text">' +
        '   <p>This content from page1(msg): </p>' +
        '   <p>{{msg}}</p>' +
        '</div>',
    props: ['msg'],
    data: function() {
        return {
            //Data
            msg: '',
            //Style
            root: 'col-lg-12',
            body: 'form-group col-xs-4',
            in_text: 'form-control',
            btn: 'btn btn-primary'
        }
    }
})
