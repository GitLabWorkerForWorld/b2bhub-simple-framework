window.onload = function() {
    function createSrc(path) {
        var script = document.createElement('script');
        script.src = path;
        bodyHtml.appendChild(script);
    }//End fo createSrc().
    var jsLib = [
        "index", "navbar", "sidebar",
        "FunA",
        "FunB", "FunB-Home", "FunB-Menu",
        "Datepicker",
        "Modal", "Modal-Login", "Modal-Home",
        "page1",
        "page2",
        "Handsontable"
    ];
    var bodyHtml = document.getElementsByTagName('body')[0];
    for(var i = 0; i < jsLib.length; i++) {
        createSrc("./tpl/" + jsLib[i] + ".js");
    }//endfor
    //Add main.js
    createSrc("./js/main.js");
}//End of window.onload().
